// practica 02 uso del objeto fetch
//manejando promesas
//manejando await
llamandoFetch =()=>{
    const url="https://jsonplaceholder.typicode.com/todos"
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data=>mostrarTodos(data))
    .catch((reject)=>{
        consolse.log("Surgio el siguiente error: "+ reject)
    });
}
const llamadoAwait=async()=>{
    try{
       const url="https://jsonplaceholder.typicode.com/todos"
        const respuesta=await fetch(url)
        const data = await respuesta.json()
        mostrarTodos(data) 
    }catch(error){
        console.log("Surgio el siguiente error: "+ reject)
    }
}
const mostrarTodos=(data)=>{
    console.log(data)
    const res = document.getElementById('respuesta');
    res.innerHTML="";
    for(let item of data){
        res.innerHTML += '<tr> <td class ="columna1">' + item.userId + '</td>'
        + '<td class ="columna2">' + item.id+ '</td>'
        + '<td class ="columna3">' + item.title + '</td>'
        + '<td class ="columna3">' + item.completed+ '</td> </tr>'
    }
    res.innerHTML +="</tbody>"
}
document.getElementById("btnCargarP").addEventListener('click', function(){
    llamandoFetch();
});
document.getElementById("btnCargarA").addEventListener('click', function(){
    llamadoAwait();
});
document.getElementById("btnLimpiar").addEventListener('click', function(){
    const res = document.getElementById('respuesta');
    res.innerHTML="";
});